let member = [
  {nim: "1703734", nama: "Dela Aledia", divisi: "Sekretaris"},
  {nim: "1702642", nama: "Reni Nuryati", divisi: "Sekretaris"},

  {nim: "1701575", nama: "Risky Mulia", divisi: "Bendahara"},
  {nim: "1705685", nama: "Vania Aprilianaa", divisi: "Bendahara"},

  {nim: "1702559", nama: "M. Habib Ibnu", divisi: "Dekorasi dan Dokumentasi"},
  {nim: "1701507", nama: "Ramdan Syaripudin", divisi: "Dekorasi dan Dokumentasi"},
  {nim: "1801341", nama: "Masyita Insyra Putri", divisi: "Dekorasi dan Dokumentasi"},
  {nim: "1804196", nama: "Imelda Widiya Hikmah", divisi: "Dekorasi dan Dokumentasi"},
  {nim: "1800799", nama: "Muhammad Biladt Hakim", divisi: "Dekorasi dan Dokumentasi"},
  {nim: "1804857", nama: "Muhammad Zahid Tsaqif ", divisi: "Dekorasi dan Dokumentasi"},
  {nim: "1702669", nama: "Willy", divisi: "Dekorasi dan Dokumentasi"},

  {nim: "1704287", nama: "Muhammad Fauzan", divisi: "Keamanan dan P3K"},
  {nim: "1802243", nama: "Tika Sartika", divisi: "Keamanan dan P3K"},
  {nim: "1601330", nama: "Renra Noviana", divisi: "Keamanan dan P3K"},
  {nim: "1807311", nama: "Kania Athallah Yuhen", divisi: "Keamanan dan P3K"},
  {nim: "1700538", nama: "Yanrie Faisal Tsaqal", divisi: "Keamanan dan P3K"},

  {nim: "1703571", nama: "Dina Dwi H", divisi: "Acara Akademik"},
  {nim: "1702092", nama: "Wawan Setiawan", divisi: "Acara Akademik"},
  {nim: "1807395", nama: "Widiyanti Hanggraeni", divisi: "Acara Akademik"},
  {nim: "1801578", nama: "Krisna Milenia", divisi: "Acara Akademik"},
  {nim: "1700138", nama: "Mia Rosmiati", divisi: "Acara Akademik"},
  {nim: "1802285", nama: "Rizki Ahmad Pauzan", divisi: "Acara Akademik"},

  {nim: "1600659", nama: "Muhammad Furqan Nur Hakim", divisi: "Logistik dan Transportasi"},
  {nim: "1804072", nama: "Musa Misbahuddin", divisi: "Logistik dan Transportasi"},
  {nim: "1801669", nama: "Muhammad Ihsan Akbar", divisi: "Logistik dan Transportasi"},
  {nim: "1705401", nama: "Muhammad Faisal Alfarizki", divisi: "Logistik dan Transportasi"},
  {nim: "1600456", nama: "Dian Permana M. D.", divisi: "Logistik dan Transportasi"},

  {nim: "1703473", nama: "Pernik Dessi Y", divisi: "Konsumsi"},
  {nim: "1700916", nama: "Althea Rizqi", divisi: "Konsumsi"},
  {nim: "1600686", nama: "Hamdan Ilham Miftahulkhoir", divisi: "Konsumsi"},
  {nim: "1805351", nama: "Nurul Afifah", divisi: "Konsumsi"},
  
  {nim: "1701112", nama: "Meirista Puspa A", divisi: "Dana Usaha dan Sponsorship"},
  {nim: "1608145", nama: "Faradissa Nurul Faidah", divisi: "Dana Usaha dan Sponsorship"},
  {nim: "1802403", nama: "Fina Royana", divisi: "Dana Usaha dan Sponsorship"},
  {nim: "1800033", nama: "Irfan Andriana", divisi: "Dana Usaha dan Sponsorship"},

  {nim: "1702658", nama: "Opik Sofian", divisi: "Hubungan Masyarakat"},
  {nim: "1807175", nama: "Aulianisa Rakhmah", divisi: "Hubungan Masyarakat"},
  {nim: "1600347", nama: "Rafli", divisi: "Hubungan Masyarakat"},
  {nim: "1705508", nama: "Taufik Nurrahman P", divisi: "Hubungan Masyarakat"},
  {nim: "1806278", nama: "Andy Fahri", divisi: "Hubungan Masyarakat"},

  {nim: "1701266", nama: "Rantty Gantini", divisi: "Publikasi"},
  {nim: "1607262", nama: "Yola Nanda Sekar P.", divisi: "Publikasi"},
  {nim: "1801612", nama: "Sidiq Nugraha", divisi: "Publikasi"},
  {nim: "1807438", nama: "Muhammad Arsyal Kyvariwijaya", divisi: "Publikasi"},
  {nim: "1700424", nama: "Rivia Putri Giovani", divisi: "Publikasi"},

  {nim: "1701113", nama: "Niko C", divisi: "Acara Sosial"},
  {nim: "1701583", nama: "Guntur Nugraha", divisi: "Acara Sosial"},
  {nim: "1804430", nama: "Non Alyya Yan Hari", divisi: "Acara Sosial"},
  {nim: "1802147", nama: "Rifqi Subagja", divisi: "Acara Sosial"},
  {nim: "1804476", nama: "Nahda Luthfia Muntaqo", divisi: "Acara Sosial"},
  {nim: "1606864", nama: "Muhammad Fahran Ramadhan", divisi: "Acara Sosial"},

  {nim: "1702897", nama: "Naufal Nur Azmi", divisi: "Acara Kerohanian"},
  {nim: "1801500", nama: "Anastasya Reskianissa", divisi: "Acara Kerohanian"},
  {nim: "1806596", nama: "Firda Faiza Hasna", divisi: "Acara Kerohanian"},
  {nim: "1808553", nama: "Meggy Nurdyansah", divisi: "Acara Kerohanian"},
  {nim: "1804795", nama: "Ghifary Daffa Pradana", divisi: "Acara Kerohanian"},
  {nim: "1600524", nama: "Hitnes Muharram", divisi: "Acara Kerohanian"},

  {nim: "1700795", nama: "Yayang Sri Marlina", divisi: "Acara"},

  {nim: "1608254", nama: "Rivaldo", divisi: "Steering Committee"},
  {nim: "1606975", nama: "Galang Satria Prasadana", divisi: "Steering Committee"},
  {nim: "1600571", nama: "Rizal Muharam", divisi: "Steering Committee"},
  {nim: "1606845", nama: "Nur Amruna Dini", divisi: "Steering Committee"},
  {nim: "1607648", nama: "Tia Herdiastuti", divisi: "Steering Committee"},

  {nim: "1608246", nama: "Ali Hasan Ash Shiddiq", divisi: "Non-Divisi"},
  {nim: "1705007", nama: "Hilmi Adlannaafi", divisi: "DPM"}
]

function isMember(arr, obj) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i].nim === obj) {
      return true;
    }
  }
}

function getName(arr, obj) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i].nim === obj) return arr[i].nama;
  }
}

function getDivision(arr, obj) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i].nim === obj) return arr[i].divisi;
  }
}

$(document).ready(function() {

  function seeResults() {
    $('form').on('submit', function(e) { //use on if jQuery 1.7+
      e.preventDefault(); //prevent form from submitting
      var nim = document.getElementById("nim").value;

      if (nim == "") {
        $(".announcement--results--text").text("Mohon masukkan NIM anda.");
      } else if (isMember(member, nim)) {
        var name = getName(member, nim);
        var division = getDivision(member, nim);
        $(".announcement--results--text").text("Selamat, " + name + " (" + nim + "), anda bergabung bersama kami di Divisi " + division + ".");
      } else {
        $(".announcement--results--text").text("Hai, NIM " + nim + "! Mohon maaf anda belum dapat bergabung sebagai panitia P2M Kemakom.");
      }

    });
  }

  seeResults();

});
